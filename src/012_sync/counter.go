package counter

import "sync"

// Counter struct
type Counter struct {
	mu    sync.Mutex
	value int
}

// Inc - increments the counter
func (c *Counter) Inc() {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.value++
}

// Value - value of the counter
func (c *Counter) Value() int {
	return c.value
}

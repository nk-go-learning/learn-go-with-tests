package wallet

import (
	"errors"
	"fmt"
)

// Bitcoin currency
type Bitcoin int

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

// Wallet struct
type Wallet struct {
	balance Bitcoin
}

// Deposit - deposit an amount into the wallet
func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

// Balance - returns the balance of the wallet
func (w *Wallet) Balance() Bitcoin {
	return w.balance
}

// ErrInsuffientFunds - insuffient funds error message
var ErrInsuffientFunds = errors.New("cannot withdraw, insufficient funds")

// Withdraw - withdraws the supplied amount
func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return ErrInsuffientFunds
	}

	w.balance -= amount
	return nil
}

package dictionary

// Dictionary map
type Dictionary map[string]string

const (
	// ErrNotFound - not found search error
	ErrNotFound = Err("could not find the word you were looking for")
	// ErrWordExists - word exists error message
	ErrWordExists = Err("can not add word because it already exists")
	// ErrWordDoesNotExist - word does not exist error message
	ErrWordDoesNotExist = Err("can not update word because it does not exist")
)

// Err - a dictionary error
type Err string

func (e Err) Error() string {
	return string(e)
}

// Search - search for a key in the dictionary
func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]

	if !ok {
		return "", ErrNotFound
	}

	return definition, nil
}

// Add - adds a word to the dictionary
func (d Dictionary) Add(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		d[word] = definition
	case nil:
		return ErrWordExists
	default:
		return err
	}

	return nil
}

// Update - updates a definition
func (d Dictionary) Update(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		return ErrWordDoesNotExist
	case nil:
		d[word] = definition
	default:
		return err
	}

	return nil
}

// Delete - deletes a word from the dictionary
func (d Dictionary) Delete(word string) {
	delete(d, word)
}

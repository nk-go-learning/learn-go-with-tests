# Learn Go With Tests

Starting to learn Go. Whilst this isn't my first attempts at writing Go code, this is something I've decided to work through to give me a good grounding and understanding of the language.

I'm following the "Learn Go With Tests" Git Book, which can be found here: https://quii.gitbook.io/learn-go-with-tests
